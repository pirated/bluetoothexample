package be.pieterdedecker.bluetoothexample.communication;

import static junit.framework.Assert.assertTrue;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import android.bluetooth.BluetoothSocket;
import android.os.AsyncTask;

/**
 * This asynchronous task maintains a live Bluetooth connection. Data
 * can be written to it from another thread and received data will be
 * passed along using AsyncTask's progress update event.
 * 
 * @author Pieter De Decker, incorporating advice from
 * https://developer.android.com/guide/topics/connectivity/bluetooth.html
 */
public class BtConnectionTask extends AsyncTask<Void, String, Void> {
	private final BluetoothSocket _btSocket;
	private final BtIncomingDataCallback _btIncomingCb;
	private final InputStream _btInStream;
	private final OutputStream _btOutStream;

	public BtConnectionTask(BluetoothSocket socket, BtIncomingDataCallback incomingCb) {
		_btSocket = socket;
		_btIncomingCb = incomingCb;

		InputStream tmpIn = null;
		OutputStream tmpOut = null;

		// Get the input and output streams, using temp objects because
		// member streams are final
		try {
			tmpIn = socket.getInputStream();
			tmpOut = socket.getOutputStream();
		} catch (IOException e) { }

		_btInStream = tmpIn;
		_btOutStream = tmpOut;
	}

	/** Task start: initiates the receive loop. */
	@Override
	protected Void doInBackground(Void... params) {
		loopReceiver();
		return null;
	}

	/** Starts receiver loop that will remain active as long as the socket exists. */
	private void loopReceiver() {
		String queue = "";
		byte prvByte = '\0';
		byte[] buffer = new byte[1024];

		// Keep listening to the InputStream until an exception occurs
		while (true) {
			try {
				// Read from the InputStream
				_btInStream.read(buffer);

				// Add buffer to queue until a \r\r is encountered
				for (int i = 0; i < buffer.length && buffer[i] != '\0'; ++i) {
					// \r\r will flush data queue
					if (buffer[i] == '\r' && prvByte == '\r') {
						// Send obtained data to observers
						publishProgress(queue);
						queue = "";
					}
					// Regular scenario: concatenate
					else {
						queue += new String(new byte[] { buffer[i] });
					}

					// Set previous byte, clear buffer
					prvByte = buffer[i];
					buffer[i] = '\0';
				}
			} catch (IOException e) {
				break;
			}
		}
	}

	/**
	 * Called on the GUI thread when we get new incoming data to trigger
	 * the incoming data callback.
	 */
	@Override
	protected void onProgressUpdate(String... strings) {
		assertTrue(strings.length == 1);
		String text = strings[0];
		_btIncomingCb.incomingData(text);
	}

	/** Sends data to the remote device. */
	public void send(String text) {
		try {
			_btOutStream.write(text.getBytes());
		} catch (IOException e) { }
	}

	/** Shuts the socket when the task is cancelled. */
	@Override
	protected void onCancelled() {
		try {
			_btSocket.close();
		} catch (IOException e) { }
	}
}