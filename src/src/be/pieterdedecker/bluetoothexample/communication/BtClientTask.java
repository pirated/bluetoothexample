package be.pieterdedecker.bluetoothexample.communication;

import static junit.framework.Assert.assertTrue;

import java.io.IOException;
import java.util.UUID;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.os.AsyncTask;

/**
 * This asynchronous task is responsible for creating an outgoing
 * Bluetooth connection to a specified device. Its post-execution
 * event will return the newly assigned Bluetooth socket, if this
 * point is ever reached. The connection process can be cancelled
 * like you would any other AsyncTask.
 * 
 * @author Pieter De Decker, incorporating advice from
 * https://developer.android.com/guide/topics/connectivity/bluetooth.html
 */
public class BtClientTask extends AsyncTask<BtConnectionEstablishedCallback, Void, BluetoothSocket> {
	private final static UUID SPP_UUID = java.util.UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
	private final BluetoothSocket _btSocket;
	private final BluetoothDevice _btDevice;
	private final BluetoothAdapter _btAdapter;
	private BtConnectionEstablishedCallback _finishCallback;

	public BtClientTask(BluetoothDevice device) {
		_btAdapter = BluetoothAdapter.getDefaultAdapter();
		assertTrue(_btAdapter != null);

		// Use a temporary object that is later assigned to the socket,
		// because the socket is final
		BluetoothSocket tmpSocket = null;
		_btDevice = device;

		// Get a BluetoothSocket to connect with the given BluetoothDevice
		try {
			// Use the Serial Port Profile
			tmpSocket = _btDevice.createRfcommSocketToServiceRecord(SPP_UUID);
		} catch (IOException e) { }
		_btSocket = tmpSocket;
	}

	@Override
	protected BluetoothSocket doInBackground(BtConnectionEstablishedCallback... params) {
		assertTrue(params.length == 1);
		_finishCallback = params[0];

		// Cancel discovery because it will slow down the connection
		_btAdapter.cancelDiscovery();

		try {
			// Connect the device through the socket. This will block
			// until it succeeds or throws an exception
			_btSocket.connect();
		} catch (IOException connectException) {
			// Unable to connect; close the socket and get out
			try {
				_btSocket.close();
			} catch (IOException closeException) { }
			return null;
		}

		return _btSocket;
	}

	/** Triggered on task cancellation: close the socket. */
	@Override
	protected void onCancelled() {
		try {
			_btSocket.close();
		} catch (IOException e) { }
	}

	/** Called when the connection has been set up. */
	@Override
	protected void onPostExecute(BluetoothSocket socket) {
		assertTrue(_finishCallback != null);
		assertTrue(socket != null);
		_finishCallback.connectionEstablished(socket);
	}
}
