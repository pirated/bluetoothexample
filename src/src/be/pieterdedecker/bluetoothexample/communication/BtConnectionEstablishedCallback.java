package be.pieterdedecker.bluetoothexample.communication;

import android.bluetooth.BluetoothSocket;

/**
 * Interface for callback that is triggered when a connection has
 * been established.  
 * @author Pieter De Decker
 */
public interface BtConnectionEstablishedCallback {
	public void connectionEstablished(BluetoothSocket socket);
}
